package org.example.todoList.model

import io.kvision.state.ObservableValue

internal data class TodoItem(
    val desc: ObservableValue<String> = ObservableValue(""),
    val completed: ObservableValue<Boolean> = ObservableValue(false),
    val selected: ObservableValue<Boolean> = ObservableValue(false)
)
