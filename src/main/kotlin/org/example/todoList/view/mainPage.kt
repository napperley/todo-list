package org.example.todoList.view

import io.kvision.Application
import io.kvision.core.AlignItems
import io.kvision.core.JustifyContent
import io.kvision.html.Align
import io.kvision.html.button
import io.kvision.html.h1
import io.kvision.panel.Root
import io.kvision.panel.root
import io.kvision.panel.vPanel
import io.kvision.state.bindEach
import io.kvision.toolbar.ButtonGroup
import io.kvision.toolbar.buttonGroup
import io.kvision.toolbar.toolbar
import io.kvision.utils.em
import org.example.todoList.model.TodoItem
import org.example.todoList.todoItems

internal fun Application.createMainPage() = root("kvapp") {
    marginTop = 2.2.em
    marginLeft = 2.2.em
    h1("Todo List") { align = Align.CENTER }
    mainPageToolbar()
    vPanel(spacing = 5, alignItems = AlignItems.CENTER) {
        marginTop = 1.0.em
        id = "todoList"
    }.bindEach(todoItems) { item -> todoListItem(item) }
}

private fun ButtonGroup.removeButton() = button("Remove") {
    id = "removeBtn"
    onClick { todoItems.removeAll(todoItems.filter { it.selected.value }) }
}

private fun ButtonGroup.unselectAllButton() = button("Unselect All") {
    id = "unselectAllBtn"
    onClick { todoItems.forEach { it.selected.value = false } }
}

private fun ButtonGroup.createButton() = button("Create") {
    id = "createBtn"
    onClick { todoItems.add(TodoItem()) }
}

private fun ButtonGroup.clearButton() = button("Clear") {
    id = "clearBtn"
    onClick { todoItems.clear() }
}

private fun ButtonGroup.toggleCompletedButton() = button("Toggle Completed") {
    id = "toggleCompletedBtn"
    onClick { todoItems.forEach { if (it.selected.value) it.completed.value = !it.completed.value } }
}

private fun Root.mainPageToolbar() = toolbar {
    justifyContent = JustifyContent.CENTER
    marginTop = 1.4.em
    buttonGroup {
        createButton()
        removeButton()
        clearButton()
    }
    buttonGroup {
        toggleCompletedButton()
        unselectAllButton()
    }
}
