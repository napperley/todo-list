package org.example.todoList.view

import io.kvision.core.AlignItems
import io.kvision.core.Container
import io.kvision.form.check.checkBox
import io.kvision.form.text.text
import io.kvision.panel.HPanel
import io.kvision.state.bind
import io.kvision.state.bindTo
import io.kvision.utils.em
import org.example.todoList.completedItemStyle
import org.example.todoList.model.TodoItem

internal class TodoListItem(val item: TodoItem) : HPanel(spacing = 2, alignItems = AlignItems.BASELINE) {
    init {
        checkBox().bindTo(item.selected)
        text {
            width = 25.em
            bind(item.completed) { completed ->
                if (completed) addCssStyle(completedItemStyle)
                else removeCssStyle(completedItemStyle)
            }
        }.bindTo(item.desc)
    }
}

internal fun Container.todoListItem(todoItem: TodoItem): TodoListItem {
    val result = TodoListItem(todoItem)
    add(result)
    return result
}
