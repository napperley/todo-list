package org.example.todoList

import io.kvision.*
import io.kvision.core.TextDecoration
import io.kvision.core.TextDecorationLine
import io.kvision.core.style
import io.kvision.state.observableListOf
import org.example.todoList.model.TodoItem
import org.example.todoList.view.createMainPage

internal val todoItems = observableListOf<TodoItem>()
val completedItemStyle = style(".completed-item") {
    style("input") {
        textDecoration = TextDecoration(line = TextDecorationLine.LINETHROUGH)
    }
}

class WebApplication : Application() {
    override fun start() {
        createMainPage()
    }
}

fun main() {
    startApplication(
        ::WebApplication,
        module.hot,
        BootstrapModule,
        BootstrapCssModule,
        CoreModule
    )
}
